import React, {useState} from 'react';
import { StyleSheet, Text, TextInput, View, Button } from 'react-native';
import {Picker} from '@react-native-picker/picker';
//import DropDownPicker from 'react-native-dropdown-picker';
import RadioForm from 'react-native-simple-radio-button';

export default function App() {

  const [weight, setWeight] = useState(0);
  const [bottles, setBottles] = useState(1);
  const [time, setTime] = useState(1);
  const [gender, setGender] = useState('male');
  const [promilles, setPromilles] = useState(0);

  const pullot=Array();
  pullot.push({label: '1 bottle', value: 1});
  pullot.push({label: '2 bottles', value: 2});
  pullot.push({label: '3 bottles', value: 3});
  pullot.push({label: '4 bottles', value: 4});
  pullot.push({label: '5 bottles', value: 5});
  pullot.push({label: '6 bottles', value: 6});
  pullot.push({label: '7 bottles', value: 7});
  pullot.push({label: '8 bottles', value: 8});
  pullot.push({label: '9 bottles', value: 9});
  pullot.push({label: '10 bottles', value: 10});
  pullot.push({label: '11 bottles', value: 11});
  pullot.push({label: '12 bottles', value: 12});

  const tunnit=Array();
  tunnit.push({label: '1 hour', value: 1});
  tunnit.push({label: '2 hours', value: 2});
  tunnit.push({label: '3 hours', value: 3});
  tunnit.push({label: '4 hours', value: 4});
  tunnit.push({label: '5 hours', value: 5});
  tunnit.push({label: '6 hours', value: 6});
  tunnit.push({label: '7 hours', value: 7});
  tunnit.push({label: '8 hours', value: 8});
  tunnit.push({label: '9 hours', value: 9});
  tunnit.push({label: '10 hours', value: 10});
  tunnit.push({label: '11 hours', value: 11});
  tunnit.push({label: '12 hours', value: 12});

  const genders = [
    {label: 'Male', value: 'male'},
    {label: 'Female', value: 'female'}
  ];

  function calculate() {
    let litres = bottles * 0.33;
    let grams = litres * 8 * 4.5;
    let burning = weight / 10;
    let endGrams = grams - burning * time;
    let result = 0;
    if (gender === 'male') {
      result = (endGrams / (weight * 0.7));
    } else {
      result = (endGrams / (weight * 0.6));
    }
    if (result < 0) {
      result = 0;
    }
    setPromilles(result);
    
  }
  

  return (
    <View style={styles.container}>
      <View style={styles.field}>
      <Text>Weight</Text>
      <TextInput
        style={styles.input}
        onChangeText={text => setWeight(text)}
        placeholder="in kilograms"
        keyboardType='numeric'>
      </TextInput>
      </View>
      <View style={styles.field}>
        <Text> Bottles</Text>
        <Picker style={styles.bottles}
          onValueChange={(itemValue) => setBottles(itemValue)}
          selectedValue={bottles}>
            {pullot.map((bottles,index) => (
              <Picker.Item key={index} label={bottles.label} value={bottles.value}/>
            ))
            }
          </Picker>
      </View>
      <View style={styles.field}>
        <Text> Time</Text>
        <Picker style={styles.time}
          onValueChange={(itemValue) => setTime(itemValue)}
          selectedValue={time}>
            {tunnit.map((time,index) => (
              <Picker.Item key={index} label={time.label} value={time.value}/>
            ))
            }
          </Picker>
      </View>
      <View style={styles.field}>
        <Text>Gender</Text>
        <RadioForm
          style={styles.radio}
          buttonSize = {10}
          radio_props={genders}
          initial={0}
          onPress={(value) => {setGender(value)}}
        />
        <Text>{promilles.toFixed(2)}</Text>
      </View>
      <Button onPress={calculate} title="Calculate"></Button>
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
  },
  field: {
    margin: 10,
  },
  input: {
    marginLeft: 10,
  },
  radio: {
    marginTop: 10,
    marginBottom: 10,
  }
});
